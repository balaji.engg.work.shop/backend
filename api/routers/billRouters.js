import {
  addNewBill,
  deleteSellTransaction,
  getBillDetails,
  getMonthlyAndTotalSell,
} from "../controllers/billController.js";
import { isAdmin, isAuth } from "../helpers/authHelper.js";
import express from "express";
const billRouters = express.Router();

// post
billRouters.post("/create", isAuth, addNewBill);
billRouters.get("/billDetails", isAuth, getBillDetails);

billRouters.get("/getCount", isAuth, getMonthlyAndTotalSell);

billRouters.delete("/deleteSellBill", isAuth, isAdmin, deleteSellTransaction);

export default billRouters;
