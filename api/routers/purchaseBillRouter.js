import {
  addNewPurchaseBill,
  getPurchaseBillDetails,
} from "../controllers/purchaseBillController.js";
import { isAuth } from "../helpers/authHelper.js";
import express from "express";
const purchaseBillRouter = express.Router();

// post
purchaseBillRouter.post("/create", isAuth, addNewPurchaseBill);

purchaseBillRouter.get("/purchaseDetails", isAuth, getPurchaseBillDetails);

export default purchaseBillRouter;
