import express from "express";
import { isAdmin, isAuth } from "../helpers/authHelper.js";
import {
  getTransactionListByCustomerId,
  getSellTransactionListByShopId,
  getTransactionListBySupplierId,
  getPurchaseTransactionListByShopId,
  getSellReturnTransactionListByCustomerId,
  getSellReturnTransactionListByShopId,
} from "../controllers/transactionController.js";

const transactionRouter = express.Router();

//get
transactionRouter.get("/customer", isAuth, getTransactionListByCustomerId);
transactionRouter.get("/supplier", isAuth, getTransactionListBySupplierId);
transactionRouter.get(
  "/customerSellReturn",
  isAuth,
  getSellReturnTransactionListByCustomerId
);
transactionRouter.get(
  "/sellReturn",
  isAuth,
  getSellReturnTransactionListByShopId
);

transactionRouter.get("/", isAuth, getSellTransactionListByShopId);
transactionRouter.get(
  "/purchaseTransactionList",
  isAuth,
  getPurchaseTransactionListByShopId
);

export default transactionRouter;
