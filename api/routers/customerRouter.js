import express from "express";
import {
  addNewCustomer,
  deleteCustomer,
  getCustomerDetailsByMobileNo,
  getCustomerListByShopId,
  updateCustomerProfile,
} from "../controllers/customerController.js";
import { isAdmin, isAuth } from "../helpers/authHelper.js";

const customerRouter = express.Router();

//post

customerRouter.post("/create", isAuth, addNewCustomer);

//get

customerRouter.get("/all", isAuth, getCustomerListByShopId);
customerRouter.get("/details", isAuth, getCustomerDetailsByMobileNo);

//put
customerRouter.put("/update", isAuth, updateCustomerProfile);

//del

customerRouter.delete("/delete", isAuth, isAdmin, deleteCustomer);

export default customerRouter;
