import express from "express";
import { createPresignedPost } from "../utils/s3.js";
import config from "../config/index.js";
import mongoose from "mongoose";

const s3Router = express.Router();

const BUCKET_NAME = config.AWS.BucketName;

s3Router.post("/signed_url", async (req, res) => {
  try {
    let { key, content_type } = req.body;
    let idname = new mongoose.Types.ObjectId();

    let fileName = String(idname);
    fileName += `.${content_type}`;
    key = `${key}/` + fileName;
    const data = await createPresignedPost({ key, contentType: content_type });
    return res.status(200).send(data);
  } catch (err) {
    console.error(err);
    return res.status(500).send({
      status: "error",
      message: err.message,
    });
  }
});
export default s3Router;
