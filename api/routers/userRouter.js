import express from "express";
import {
  updateUser,
  addNewUser,
  userSignIn,
  deleteUserTemporary,
  getUserDetail,
  getUserListByShopId,
  updateUserByAdmin,
  deleteUserPermanent,
  changePassword,
  googleSignIn,
  sendEmailToSetPassword,
  forgetPassword,
} from "../controllers/userControllers.js";
import { isAdmin, isAuth } from "../helpers/authHelper.js";

const userRouter = express.Router();

//post
userRouter.post("/signIn", userSignIn); // tested
userRouter.post("/googleSignIn", googleSignIn); // tested

userRouter.post("/create", isAuth, isAdmin, addNewUser);
userRouter.post("/sendEmailToSetPassword", sendEmailToSetPassword);

//get
userRouter.get("/userDetail", isAuth, getUserDetail);
userRouter.get("/userListByShopId", isAuth, isAdmin, getUserListByShopId);

//put
userRouter.put("/updateProfile", isAuth, updateUser);
userRouter.put("/updateByAdmin", isAuth, isAdmin, updateUserByAdmin);
userRouter.put("/changePassword", isAuth, changePassword);
userRouter.put("/setNewPassword", forgetPassword);

//del
userRouter.delete("/deleteTemp", isAuth, isAdmin, deleteUserTemporary);
userRouter.delete("/deletePermanent", isAuth, isAdmin, deleteUserPermanent);

export default userRouter;
