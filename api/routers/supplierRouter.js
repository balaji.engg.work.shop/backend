import express from "express";
import {
  addNewSupplier,
  getSupplierBySupplierId,
  getSupplierListByShopId,
  updateSupplierProfile,
} from "../controllers/supplierController.js";
import { isAuth } from "../helpers/authHelper.js";

const supplierRouter = express.Router();

//post

supplierRouter.post("/create", isAuth, addNewSupplier);

//get

supplierRouter.get("/all", isAuth, getSupplierListByShopId);
supplierRouter.get("/details", isAuth, getSupplierBySupplierId);

//put
supplierRouter.put("/update", updateSupplierProfile);

//del

export default supplierRouter;
