import express from "express";
import { isAdmin, isAuth } from "../helpers/authHelper.js";
import {
  addNewBankAccount,
  deleteBankAccount,
  getBankAccountListByShopId,
  updateBankAccountDetails,
} from "../controllers/bankAccountController.js";

const backAccountRouter = express.Router();

// post

backAccountRouter.post("/create", isAuth, isAdmin, addNewBankAccount);
// get
backAccountRouter.get(
  "/bankAccountList",
  isAuth,
  isAdmin,
  getBankAccountListByShopId
);

// put
backAccountRouter.put("/update", isAuth, isAdmin, updateBankAccountDetails);

// delete
backAccountRouter.delete("/delete", isAuth, isAdmin, deleteBankAccount);

export default backAccountRouter;
