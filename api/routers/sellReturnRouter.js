import express from "express";
import { isAuth } from "../helpers/authHelper.js";
import { addSellReturn, getSellReturnDetails } from "../controllers/sellReturnController.js";

const sellReturnRouter = express.Router();

// post

sellReturnRouter.post("/addProductsReturn", isAuth, addSellReturn);

// get
sellReturnRouter.get("/getSellReturnDetails", isAuth, getSellReturnDetails);


export default sellReturnRouter;
