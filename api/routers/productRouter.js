import express from "express";
import {
  addNewProduct,
  addProductInBulk,
  dailyManufactureProducts,
  deleteAllProducts,
  deleteProduct,
  getAllProductByShopId,
  getAllProductList,
  getDailyManufactureProductList,
  getProductDetails,
  getProductListByCategory,
  productListByProductName,
  updatedProduct,
} from "../controllers/productController.js";
import { isAuth } from "../helpers/authHelper.js";

const productRouter = express.Router();

//post

productRouter.post("/create", isAuth, addNewProduct);
productRouter.post("/addInBulk", isAuth, addProductInBulk);

//get
productRouter.get("/all", getAllProductList);

productRouter.get("/productListByShopId", getAllProductByShopId);
productRouter.get("/details", isAuth, getProductDetails);
productRouter.get("/:category/:shopId", isAuth, getProductListByCategory);
productRouter.get(
  "/dailyManufactureList",
  isAuth,
  getDailyManufactureProductList
);

productRouter.get(
  "/productListByProductName",
  isAuth,
  productListByProductName
);

//put
productRouter.put("/update", isAuth, updatedProduct);
productRouter.put("/dailyManufacture", isAuth, dailyManufactureProducts);

//del
productRouter.delete("/delete", isAuth, deleteProduct);
productRouter.delete("/deleteAll", isAuth, deleteAllProducts);

export default productRouter;
