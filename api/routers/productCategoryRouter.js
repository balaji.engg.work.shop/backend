import express from "express";
import { isAuth } from "../helpers/authHelper.js";
import {
  addNewProductCategory,
  getProductCategory,
} from "../controllers/productCategoryController.js";

const productCategoryRouter = express.Router();

// get
productCategoryRouter.get("/", isAuth, getProductCategory);

// post
productCategoryRouter.post("/create", isAuth, addNewProductCategory);

// put

// productCategoryRouter.put("/update", isAuth, updateProductCategory);

export default productCategoryRouter;
