import express from "express";
import {
  createNewShop,
  getShopDetails,
  getShopList,
  updatedShopDetails,
} from "../controllers/shopControllers.js";
import { isAdmin, isAuth } from "../helpers/authHelper.js";
const shopRouter = express.Router();

//get
shopRouter.get("/", isAuth, getShopDetails); // tested postman
shopRouter.get("/all", getShopList); // tested postman


//post
shopRouter.post("/create", createNewShop); // tested postman

//put
shopRouter.put("/update", isAuth, isAdmin, updatedShopDetails); // tested postman

export default shopRouter;
