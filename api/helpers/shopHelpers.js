import Shop from "../models/shopModel.js";

export const createNewShopHelper = async (shopId, req) => {
  try {
    const newShop = new Shop({
      _id: shopId,
      shopId: shopId,
      shopName: req.body.shopName,
      users: [],
      ownerName: req.body.ownerName,
      shopEmail: req.body.email.trim().toLowerCase(),
      shopMobileNo: req.body.mobileNo,
    });
    const shop = await newShop.save();
    return shop;
  } catch (error) {
    throw new Error(error);
  }
};
