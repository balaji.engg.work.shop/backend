import CashAccount from "../models/cashAccountModel.js";
import mongoose from "mongoose";
export const incrementBalanceInCaseAccount = async (cashAccountNo, value) => {
  try {
    console.log("incrementBalanceInCaseAccount ", cashAccountNo, value);

    const oldProduct = await CashAccount.updateOne(
      {
        cashAccountNo: cashAccountNo,
      },
      { $inc: { currentBalance: Number(value) } }
    );
    return oldProduct;
  } catch (error) {
    throw new Error(error);
  }
};

export const decrementBalanceInCaseAccount = async (cashAccountNo, value) => {
  try {
    console.log("decrementBalanceInCaseAccount ", cashAccountNo, value);
    const oldProduct = await CashAccount.updateOne(
      {
        cashAccountNo: cashAccountNo,
      },
      { $inc: { currentBalance: -1 * Number(value) } }
    );
    return oldProduct;
  } catch (error) {
    throw new Error(error);
  }
};

export const createCashAccountForShop = async (shopId) => {
  try {
    const id = new mongoose.Types.ObjectId();
    const myCashAccount = new CashAccount({
      shopId,
      cashAccountNo: id,
      _id: id,
    });
    const createdCashAccount = await myCashAccount.save();
    return createdCashAccount;
  } catch (error) {
    throw new Error(error);
  }
};
