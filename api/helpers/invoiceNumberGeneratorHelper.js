import InvoiceNumber, {
  SellReturnInvoiceNumber,
} from "../models/invoiceNumberModel.js";

export async function getNextInvoiceNumber(shopId) {
  const counter = await InvoiceNumber.findByIdAndUpdate(
    { _id: shopId },
    { $inc: { sequence_value: 1 } },
    { new: true, upsert: true }
  );
  console.log("getNextInvoiceNumber : ", counter.sequence_value);

  return counter.sequence_value;
}

export async function getNextInvoiceNumberForSellReturn(shopId) {
  const counter = await SellReturnInvoiceNumber.findByIdAndUpdate(
    { _id: shopId },
    { $inc: { sequence_value: 1 } },
    { new: true, upsert: true }
  );

  return counter.sequence_value;
}
