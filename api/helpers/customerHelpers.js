import Customer from "../models/customerModel.js";

export const incrementCustomerPurchaseAmount = async (customerId, value) => {
  try {
    const oldCustomer = await Customer.updateOne(
      {
        customerId: customerId,
      },
      { $inc: { totalPurchaseAmount: Number(value) } }
    );
    return oldCustomer;
  } catch (error) {
    throw new Error(error);
  }
};

export const decrementCustomerPurchaseAmount = async (customerId, value) => {
  try {
    const oldCustomer = await Customer.updateOne(
      {
        customerId: customerId,
      },
      { $inc: { totalPurchaseAmount: -1 * Number(value) } }
    );
    return oldCustomer;
  } catch (error) {
    throw new Error(error);
  }
};
