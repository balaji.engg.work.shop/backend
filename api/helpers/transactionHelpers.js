import mongoose from "mongoose";
import Transaction from "../models/transactionModel.js";
import { CREDIT, ONLINE } from "../Constants/genericConstant.js";
import {
  decrementBalanceInAccount,
  incrementBalanceInAccount,
} from "./bankAccountHelpers.js";
import {
  decrementBalanceInCaseAccount,
  incrementBalanceInCaseAccount,
} from "./caseAccountHelper.js";
import CashAccount from "../models/caseAccountModel.js";

export const addNewTransaction = async (data) => {
  try {
    const transactionId = new mongoose.Types.ObjectId();
    const newTransaction = new Transaction({
      _id: transactionId,
      transactionId: transactionId,
      bankTransactionId: data.bankTransactionId,
      billNo: data.billNo,
      billAmount: data.billAmount,
      paidAmount: data.paidAmount,
      mot: data.mot,
      accountNumber: data.accountNumber,
      customerId: data.customerId,
      transactionType: data.transactionType, //  CREDIT, DEBIT
      shopId: data.shopId,
    });

    const saveTransaction = await newTransaction.save();

    // if mot id online and transactionType call increment amount in bank account or case account
    if (data.mot === ONLINE) {
      if (data.transactionType === CREDIT) {
        await incrementBalanceInAccount(data.accountNumber, data.paidAmount);
      } else {
        await decrementBalanceInAccount(data.accountNumber, data.paidAmount);
      }
    } else {
      const caseAccount = await CashAccount.find({ shopId: data.shopId });
      if (data.transactionType === CREDIT) {
        await incrementBalanceInCaseAccount(
          caseAccount.cashAccountNo,
          data.paidAmount
        );
      } else {
        await decrementBalanceInCaseAccount(
          caseAccount.cashAccountNo,
          data.paidAmount
        );
      }
    }
  } catch (error) {}
};
