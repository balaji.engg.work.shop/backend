import BankAccount from "../models/bankAccountModel.js";

export const incrementBalanceInAccount = async (accountNumber, value) => {
  try {
    console.log("incrementBalanceInAccount : ", accountNumber, value);
    const oldProduct = await BankAccount.updateOne(
      {
        accountNumber: accountNumber,
      },
      { $inc: { currentBalance: Number(value) } }
    );
    return oldProduct;
  } catch (error) {
    throw new Error(error);
  }
};

export const decrementBalanceInAccount = async (accountNumber, value) => {
  try {
    console.log("decrementBalanceInAccount : ", accountNumber, value);
    const oldProduct = await BankAccount.updateOne(
      {
        accountNumber: accountNumber,
      },
      { $inc: { currentBalance: -1 * Number(value) } }
    );
    return oldProduct;
  } catch (error) {
    throw new Error(error);
  }
};
