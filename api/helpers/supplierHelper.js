import Supplier from "../models/supplierModel.js";

export const incrementSupplierPurchaseAmount = async (supplierId, value) => {
  try {
    console.log("incrementSupplierPurchaseAmount : ", supplierId, value);
    const oldSupplier = await Supplier.updateOne(
      {
        supplierId: supplierId,
      },
      { $inc: { totalPurchaseAmount: Number(value) } }
    );
    return oldSupplier;
  } catch (error) {
    throw new Error(error);
  }
};
