import mongoose from "mongoose";
import { ADMIN } from "../Constants/userConstant.js";
import User from "../models/userModel.js";
import { generateToken } from "./authHelper.js";
import { bcryptPassword } from "../utils/bcrypt.js";

export const createNewAdminUser = async (shop, req) => {
  try {
    const usedId = new mongoose.Types.ObjectId();
    const newUser = new User({
      userId: usedId,
      _id: usedId,
      userName: req.body.email.split("@")[0],
      userRole: ADMIN,
      fullName: req.body.ownerName,
      password: bcryptPassword(req.body.password),
      shopId: shop.shopId,
      mobileNo: req.body.mobileNo,
      email: req.body.email.toLowerCase(),
      isAdmin: true,
      shopName: shop?.shopName,
    });
    const user = await newUser.save();
    return user;
  } catch (error) {
    throw new Error(error);
  }
};

export const returnStaffSignIn = (user) => {
  console.log("returnStaffSignIn called!");
  return {
    usedId: user.usedId,
    fullName: user.fullName,
    email: user.email,
    mobileNo: user.mobileNo,
    avatar: user.avatar,
    userRole: user.userRole,
    isAdmin: user.isAdmin,
    isStaff: user.isStaff,
    shopId: user.shopId,
    shopName: user.shopName,
    gender: user.gender,
    aadhaarNo: user.aadhaarNo,
    token: generateToken(user),
    createdAt: user.createdAt,
  };
};

export const updateUserDetailsHelper = async (userId, req) => {
  try {
    console.log("updateUserDetailsHelper : ", userId, req.body);
    const user = await User.findById(userId);
    if (!user) throw new Error("User not found!");

    user.fullName = req.body.fullName || user.fullName;
    user.email =
      req.body.email.trim().toLowerCase() || user.email.trim().toLowerCase();
    user.mobileNo = req.body.mobileNo || user.mobileNo;
    user.userName = req.body.userName || user.userName;
    user.aadhaarNo = req.body.aadhaarNo || user.aadhaarNo;
    user.avatar = req.body.avatar || user.avatar;
    user.gender = req.body.gender || user.gender;
    user.address = req.body.address || user.address;
    user.district = req.body.district || user.district;
    user.state = req.body.state || user.state;
    user.country = req.body.country || user.country;
    user.pinCode = req.body.pinCode || user.pinCode;

    const updatedStaff = await user.save();
    return updatedStaff;
  } catch (error) {
    console.log("error", error);
    throw new Error(error);
  }
};
