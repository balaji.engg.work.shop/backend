export const getBillNumber = (billCount) => {
  let obj = new Date();
  let year = obj.getUTCFullYear();
  return `INV-${year}-${billCount}`;
};

export const getSellReturnBillNumber = (billCount) => {
  let obj = new Date();
  let year = obj.getUTCFullYear();
  return `INV-R-${year}-${billCount}`;
};
