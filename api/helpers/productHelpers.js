import Product from "../models/productModel.js";

export const incrementProductQuantity = async (productId, value) => {
  try {
    const oldProduct = await Product.updateOne(
      {
        productId: productId,
      },
      { $inc: { quantity: Number(value) } }
    );
    return oldProduct;
  } catch (error) {
    throw new Error(error);
  }
};

export const decrementProductQuantity = async (productId, value) => {
  try {
    const oldProduct = await Product.updateOne(
      {
        productId: productId,
      },
      { $inc: { quantity: -1 * Number(value) } }
    );
    return oldProduct;
  } catch (error) {
    throw new Error(error);
  }
};
