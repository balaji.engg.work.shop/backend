import jwt from "jsonwebtoken";
import { mySecreteKey } from "../config/secretKey.js";

// to keep logged in info
export const generateToken = (user) => {
  console.log("generateToken called!");
  return jwt.sign(
    {
      userId: user.userId,
      fullName: user.fullName,
      email: user.email,
      mobileNo: user.mobileNo,
      avatar: user.avatar,
      userRole: user.userRole,
      isAdmin: user.isAdmin,
      isStaff: user.isStaff,
      shopId: user.shopId,
      shopName: user.shopName,
    },
    process.env.ACCESS_TOKEN_JWT_SECRET || mySecreteKey,
    {
      expiresIn: "10h",
    }
  );
};

// to authenticate user
export const isAuth = (req, res, next) => {
  const authorization =
    req.headers.authorization || req.body.headers.authorization;
  if (authorization) {
    const token = authorization.slice(7, authorization.length); //Bearer XXXXXXXX... (removes Bearer_ and gives token XXXXXXXXX...)
    jwt.verify(
      token,
      process.env.ACCESS_TOKEN_JWT_SECRET || mySecreteKey,
      (err, decode) => {
        if (err) {
          console.log("isAuth : error");
          return res
            .status(404)
            .send({ message: "Please Sign in Again to continue" });
        } else {
          req.user = decode;
          next();
          return;
        }
      }
    );
  } else {
    return res.status(401).send({ message: "Sign in issue" });
  }
};

export const isAdmin = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next();
  } else {
    res.status(401).send({
      message: "You have no access!",
    });
  }
};

export const isStaff = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next();
  } else {
    res.status(401).send({
      message: "You have no access of staff!",
    });
  }
};
