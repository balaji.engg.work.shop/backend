import mongoose from "mongoose";
import mongodb from "mongodb";

export const createConnection = async (url) => {
  try {
    await mongoose.connect(url);
    console.log("Connected to Database");
  } catch (error) {
    console.log("Connection not found!.........", error);
  }
};
