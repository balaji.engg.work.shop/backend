export const PER_PIECE = "Per piece";
export const PER_KG = "Per KG";

export const SAVING = "Saving";
export const CURRENT = "Current";

export const ONLINE = "Online";
export const CASH = "Cash";
export const BOTH = "Both";

export const CREDIT = "CREDIT";
export const DEBIT = "DEBIT";

export const SELL_PRODUCT = "Sell Products";
export const SELL_RETURN = "Sell Return";
export const PURCHASE_PRODUCT = "Purchase Products";
export const OTHERS = "Others";
