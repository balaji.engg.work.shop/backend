import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import shopRouter from "./routers/shopRouters.js";
import { createConnection } from "./db/index.js";
import * as dotenv from "dotenv";
import userRouter from "./routers/userRouter.js";
import productRouter from "./routers/productRouter.js";
import customerRouter from "./routers/customerRouter.js";
import { dbURL, localDBUrl } from "./config/dbConfig.js";
import productCategoryRouter from "./routers/productCategoryRouter.js";
import backAccountRouter from "./routers/bankAccount.js";
import billRouter from "./routers/billRouters.js";
import transactionRouter from "./routers/transactionRouter.js";
import path from "path";
import { fileURLToPath } from "url";
import { dirname } from "path";
import fs from "fs";
import * as http from "http";
import https from "https";
import supplierRouter from "./routers/supplierRouter.js";
import purchaseBillRouter from "./routers/purchaseBillRouter.js";
import sellReturnRouter from "./routers/sellReturnRouter.js";
import s3Router from "./routers/s3Routers.js";
import config from "./config/index.js";


const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

dotenv.config();

console.log("dd", __dirname);

// create connection with database
createConnection(dbURL);

const app = express();
const PORT = config.PORT;

// middle-where
app.use(express.json({ limit: "100mb", extended: true }));
app.use(express.urlencoded({ limit: "100mb", extended: true }));
app.use(cors({ origin: "*" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// testing
app.get("/", (req, res) => {
  return res.json({
    message: "Every thing running file",
  });
});

app.get("/health", (req, res) => {
  return res.json({
    message: "Healthy application",
  });
});

app.post("/", (req, res) => {
  return res.json({
    message: "Every thing running file post request",
  });
});

const key = fs.readFileSync(path.resolve(__dirname, "../private.key"));
const cert = fs.readFileSync(path.resolve(__dirname, "../certificate.crt"));

const cred = {
  key,
  cert,
};

app.use("/api/v1/shops", shopRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/products", productRouter);
app.use("/api/v1/customers", customerRouter);
app.use("/api/v1/productCategory", productCategoryRouter);
app.use("/api/v1/bankAccount", backAccountRouter);
app.use("/api/v1/bill", billRouter);
app.use("/api/v1/transaction", transactionRouter);
app.use("/api/v1/suppliers", supplierRouter);
app.use("/api/v1/transaction", transactionRouter);
app.use("/api/v1/purchaseBill", purchaseBillRouter);
app.use("/api/v1/sellReturn", sellReturnRouter);
app.use("/api/v1/s3", s3Router);

app.use(
  "/.well-known/pki-validation/9E1E0ABD00ED6AAC802E6DE7E35AB4E3.txt",
  (req, res) => {
    res.sendFile(
      path.resolve(__dirname, "../9E1E0ABD00ED6AAC802E6DE7E35AB4E3.txt")
    );
  }
);

// app.listen(PORT, () => {
//   console.log(`http://localhost:${PORT}`);
// });
const server = http.createServer(app);
server.listen(PORT, "0.0.0.0", () => {
  console.log("Server started on port  : ", PORT);
});

const httpServer = https.createServer(cred, app);
httpServer.listen(8443);
