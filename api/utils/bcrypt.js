import bcrypt from "bcryptjs";

export const bcryptPassword = (password) => {
  return bcrypt.hashSync(password, 16);
};

export const comparePassword = (oldPassword, newPassword) => {
  return bcrypt.compareSync(oldPassword, newPassword);
};
