export const transaction = {
  transactionId: 1,
  transactionDate: 1,
  bankTransactionId: 1,
  accountNumber: 1,
  billNo: 1,
  billAmount: 1,
  paidAmount: 1,
  mot: 1, // mode of transaction
  customerId: 1,
  customerName: 1,
  supplierId: 1,
  supplierName: 1,
  transactionType: 1,
  userId: 1,
  userName: 1,
};
