import mongoose from "mongoose";
import Shop from "../models/shopModel.js";
import User from "../models/userModel.js";
import { createNewShopHelper } from "../helpers/shopHelpers.js";
import { createNewAdminUser } from "../helpers/userHelper.js";
import { createCashAccountForShop } from "../helpers/cashAccountHelper.js";

export const createNewShop = async (req, res) => {
  try {
    console.log("new shop create request called!");
    const oldShop = await Shop.findOne({
      $or: [
        { shopEmail: req.body.email.trim().toLowerCase() },
        { shopMobileNo: req.body.mobileNo },
      ],
    });
    console.log("shop : ", oldShop);
    if (oldShop)
      return res.status(400).send({
        message:
          "You can't create shop and user with this email or mobile no, try with different email or mobile",
      });
    const shopId = new mongoose.Types.ObjectId();
    const shop = await createNewShopHelper(shopId, req);
    const newUser = await createNewAdminUser(shop, req);
    const shopCashAccount = await createCashAccountForShop(shopId);

    shop.users.push(newUser.userId);
    const updatedShop = await shop.save();
    console.log("Created shop, user ", { shop, newUser });
    return res.status(201).send({ message: "Shop created successfully!" });
  } catch (error) {
    console.log("error : ", error);
    return res.status(500).send({ message: error });
  }
};

export const getShopDetails = async (req, res) => {
  try {
    console.log("getShopDetails called ,", req.query.shopId);
    const shopId = req.user.shopId;
    const shop = await Shop.findById(shopId);
    if (shop) return res.status(200).send(shop);
    return res.status(404).send({ message: "Shop not found" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in getShopDetails controller ${error}` });
  }
};

export const getShopList = async (req, res) => {
  try {
    const shop = await Shop.find();
    if (shop.length) return res.status(200).send(shop);
    return res.status(404).send({ message: "No shop found" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in getShopDetails controller ${error}` });
  }
};

export const updatedShopDetails = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const shop = await Shop.findById(shopId);

    if (!shop) return res.status(404).send({ message: "Shop not found" });

    shop.shopName = req.body.shopName || shop.shopName;
    shop.shopEmail =
      req.body.shopEmail.trim().toLowerCase() ||
      shop.shopEmail.trim().toLowerCase();
    shop.address = req.body.address || shop.address;
    shop.city = req.body.city || shop.city;
    shop.state = req.body.state || shop.state;
    shop.country = req.body.country || shop.country;
    shop.pinCode = req.body.pinCode || shop.pinCode;
    shop.gst = req.body.gst || shop.gst;
    shop.ownerName = req.body.ownerName || shop.ownerName;
    shop.mobileNo = req.body.mobileNo || shop.mobileNo;
    shop.shopOpeningTime = req.body.shopOpeningTime || shop.shopOpeningTime;
    shop.shopClosingTime = req.body.shopClosingTime || shop.shopClosingTime;
    shop.shopLogo = req.body.shopLogo || shop.shopLogo;
    shop.shopImages = req.body.shopImages || shop.shopImages;
    shop.geoLocation = req.body.geoLocation || shop.geoLocation;
    const updatedShop = await shop.save();
    console.log("shop updated! 11", updatedShop);

    return res.status(200).send(updatedShop);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in updatedShopDetails controller ${error}` });
  }
};

export const deleteShopPermanently = async (req, res) => {
  try {
    console.log("deleteShopPermanently called!");
    const shopId = req.params.shopId;
    const shop = await Shop.findById(shopId);
    // delete all user related to this shop

    // delete all product list related to this shop

    // delete all bank account related to this shop

    // delete all bank account transaction related to this shop

    // delete  shop itself

    return res.status(200).send("Deleted shop!");
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in updatedShopDetails controller ${error}` });
  }
};
