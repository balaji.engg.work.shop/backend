import { incrementProductQuantity } from "../helpers/productHelpers.js";
import Shop from "../models/shopModel.js";
import Transaction from "../models/transactionModel.js";
import PurchaseBill from "../models/purchaseBillModel.js";
import { incrementSupplierPurchaseAmount } from "../helpers/supplierHelper.js";
import {
  BOTH,
  CASH,
  DEBIT,
  ONLINE,
  PURCHASE_PRODUCT,
} from "../Constants/genericConstant.js";
import mongoose from "mongoose";
import {
  decrementBalanceInAccount,
  incrementBalanceInAccount,
} from "../helpers/bankAccountHelpers.js";
import {
  createCashAccountForShop,
  decrementBalanceInCaseAccount,
  incrementBalanceInCaseAccount,
} from "../helpers/cashAccountHelper.js";
import CashAccount from "../models/cashAccountModel.js";
import Supplier from "../models/supplierModel.js";

export const addNewPurchaseBill = async (req, res) => {
  try {
    const shop = await Shop.findById(req.user.shopId);

    const transactionId = new mongoose.Types.ObjectId();
    const id = new mongoose.Types.ObjectId();
    let newBill = new PurchaseBill({
      _id: id,
      billNo: req.body.billNo,
      billDate: req.body.billDate || new Date(),
      shopId: req.user.shopId,
      userId: req.user.userId,
      supplierId: req.body.supplierId,
      modeOfTransaction: req.body.modeOfTransaction, // Case/ Online , both
      total: req.body.total, // without gst
      gstPercent: req.body.gstPercent,
      gstAmount: req.body.gstAmount,
      discountAmount: req.body.discountAmount,
      netTotal: req.body.netTotal,
      onlineAmount: req.body.onlineAmount,
      cashAmount: req.body.cashAmount,
      paidAmount: req.body.paidAmount,
      products: req.body.products,
      transactionId: transactionId,
    });

    const newTransaction = new Transaction({
      _id: transactionId,
      transactionId: transactionId,
      bankTransactionId: req.body.bankTransactionId,
      billNo: newBill.billNo,
      billAmount: req.body.netTotal,
      paidAmount: req.body.paidAmount,
      mot: req.body.modeOfTransaction,
      accountNumber: req.body.accountNumber,
      transactionType: req.body.transactionType || DEBIT, //  CREDIT, DEBIT
      shopId: req.user.shopId,
      supplierId: req.body.supplierId,
      supplierName: req.body.supplierName,
      userId: req.user.userId,
      userName: req.user.fullName,
      transactionPurpose: PURCHASE_PRODUCT,
    });
    const transaction = await newTransaction.save();
    newBill = await newBill.save();

    // if mot id online and transactionType call increment amount in bank account or case account
    if (
      req.body.modeOfTransaction === ONLINE ||
      req.body.modeOfTransaction === BOTH
    ) {
      await decrementBalanceInAccount(
        req.body.accountNumber,
        req.body.onlineAmount
      );
    }

    if (
      req.body.modeOfTransaction === CASH ||
      req.body.modeOfTransaction === BOTH
    ) {
      const caseAccount = await CashAccount.findOne({
        shopId: req.user.shopId,
      });
      console.log("caseAccount ", caseAccount);
      if (!caseAccount) {
        // create cash account for this shop
        const shopCashAccount = await createCashAccountForShop(req.user.shopId);
        console.log("new caseAccount ", shopCashAccount);

        await decrementBalanceInCaseAccount(
          shopCashAccount.cashAccountNo,
          req.body.cashAmount
        );
      } else {
        await decrementBalanceInCaseAccount(
          caseAccount.cashAccountNo,
          req.body.cashAmount
        );
      }
    }

    await incrementSupplierPurchaseAmount(
      req.body.supplierId,
      req.body.paidAmount
    );

    // now update product quantity
    for (let product of req.body.products) {
      if (product.productId) {
        const response = await incrementProductQuantity(
          product.productId,
          product.quantity
        );
      }
    }

    console.log("result:", {
      bill: newBill,
      shop: shop,
      transaction,
    });

    // now send the response
    return res.status(201).send({
      bill: newBill,
      shop: shop,
    });
  } catch (error) {
    console.log("Error in addNewBill", error);
    return res.status(500).send({ message: error });
  }
};

export const getPurchaseBillDetails = async (req, res) => {
  try {
    const bill = await PurchaseBill.findOne({
      shopId: req.user.shopId,
      billNo: req.query.billNo,
    });
    if (!bill) {
      return res.status(404).send({ message: "No bill detail found!" });
    }
    const shop = await Shop.findById(req.user.shopId);
    const customer = await Supplier.findById(bill.supplierId);
    return res.status(200).send({ bill, shop, customer });
  } catch (error) {
    console.log("Error in addNewBill", error);
    return res.status(500).send({ message: error });
  }
};
