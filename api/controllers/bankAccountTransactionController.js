import { CREDIT } from "../Constants/transaction.js";
import BankAccount from "../models/bankAccountModel";
import BankAccountTransaction from "../models/bankAccountTransactionModel.js";

export const addNewBankAccountTransaction = async (req, res) => {
  try {
    const bankAccountId = req.params.bankAccountId;
    const bankAccount = await BankAccount.findById(bankAccountId);

    const newTransaction = new BankAccountTransaction({
      bankAccount: bankAccountId,
      date: req.body.date || new Date(),
      amount: req.body.amount,
      transactionType: req.body.transactionType,
      currentBalance:
        req.body.transactionType === CREDIT
          ? bankAccount.currentBalance + req.body.amount
          : bankAccount.currentBalance - req.body.amount,
      transactionsId: req.body.transactionsId,
      description: req.body.description,
      customer: req.body.customer,
      supplier: req.body.supplier,
      purpose: req.body.purpose, // customer, supplier , other
    });

    const saveTransaction = await newTransaction.save();
    // update bankAccount currentBalance also
    bankAccount.currentBalance = saveTransaction.currentBalance;
    await bankAccount.save();
    console.log("new transaction saved : ", saveTransaction);
    return res.status(200).send(saveTransaction);
  } catch (error) {
    console.log("Error in addNewBankAccountTransaction: ", error);
    return res
      .status(500)
      .send({ message: `Error in addNewBankAccountTransaction ${error}` });
  }
};
