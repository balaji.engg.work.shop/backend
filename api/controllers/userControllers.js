import mongoose from "mongoose";
import {
  returnStaffSignIn,
  updateUserDetailsHelper,
} from "../helpers/userHelper.js";
import User from "../models/userModel.js";
import { bcryptPassword } from "../utils/bcrypt.js";
import bcrypt from "bcryptjs";
import { sendConfirmationEmail } from "../utils/sendEmail.js";

export const userSignIn = async (req, res) => {
  try {
    const userName = req.body.userName;

    const user = await User.findOne({
      $or: [
        { email: userName.trim().toLowerCase() },
        { userName: userName.trim().toLowerCase() },
      ],
    });

    if (user && bcrypt.compareSync(req.body.password, user.password)) {
      console.log("login success!");
      return res.status(200).send(returnStaffSignIn(user));
    }
    console.log("login failed!");
    return res.status(404).send({ message: "Username or password mismatch" });
  } catch (error) {
    console.log("Error in sign in : ", error);
    return res
      .status(500)
      .send({ message: `Error in userSignIn controller ${error}` });
  }
};
export const googleSignIn = async (req, res) => {
  try {
    const email = req.body.email;

    console.log("googleSignIn : ", email);

    const user = await User.findOne({
      email: email.trim().toLowerCase(),
    });

    if (user) {
      console.log("login success!");
      return res.status(200).send(returnStaffSignIn(user));
    }
    console.log("login failed!");
    return res.status(404).send({ message: "Username or password mismatch" });
  } catch (error) {
    console.log("Error in sign in : ", error);
    return res
      .status(500)
      .send({ message: `Error in userSignIn controller ${error}` });
  }
};

export const addNewUser = async (req, res) => {
  try {
    const email = req.body.email.trim().toLowerCase();

    const oldUser = await User.findOne({ email: email });
    if (oldUser) return res.status(400).send("User exist with same name");

    const id = new mongoose.Types.ObjectId();
    const newUser = new User({
      userId: id,
      _id: id,
      fullName: req.body.fullName,
      email: email,
      mobileNo: req.body.mobileNo,
      password: bcryptPassword(req.body.password),
      userName: req.body.userName || email.split("@")[0],
      shopId: req.user.shopId,
      shopName: req.user.shopName,
      gender: req.body.gender,
      aadhaarNo: req.body.aadhaarNo,
    });

    const savedUser = await newUser.save();
    console.log("user created successfully : ", savedUser);
    return res.status(201).send(savedUser);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in addNewUser controller ${error}` });
  }
};

export const deleteUserTemporary = async (req, res) => {
  try {
    const userId = req.query.userId;
    const deletedStaff = await User.updateOne(
      { userId },
      { $set: { isActive: false } }
    );
    console.log("deleted user : ", deletedStaff);
    return res.status(200).send({ message: "User delete temporary" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in deleteUserTemporary controller ${error}` });
  }
};
// deleteUserPermanent

export const deleteUserPermanent = async (req, res) => {
  try {
    const userId = req.query.userId;
    const deletedStaff = await User.deleteOne({ userId });
    console.log("deleteUserPermanent user : ", deletedStaff);
    return res.status(200).send({ message: "User delete permanently" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in deleteUserTemporary controller ${error}` });
  }
};

export const makeActiveStaff = async (req, res) => {
  try {
    const userId = req.params.userId;
    const deletedStaff = await User.updateOne(
      { userId },
      { $set: { isActive: true } }
    );
    console.log("deleted user : ", deletedStaff);
    return res.status(200).send("User active now");
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in deleteUserTemporary controller ${error}` });
  }
};

export const updateUserByAdmin = async (req, res) => {
  try {
    console.log("updateUserByAdmin called!");
    const updatedStaff = await updateUserDetailsHelper(req.body.userId, req);
    console.log("updated user : ", updatedStaff);
    return res.status(200).send(updatedStaff);
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send({ message: `Error in updateUser controller ${error}` });
  }
};

// updated by user
export const updateUser = async (req, res) => {
  try {
    console.log("updateUser : ", req.user.userId);
    const user = await updateUserDetailsHelper(req.user.userId, req);
    console.log("updated user : ", user);
    return res.status(200).send(returnStaffSignIn(user));
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send({ message: `Error in updateUser controller ${error}` });
  }
};

export const getUserDetail = async (req, res) => {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) return res.status(404).send({ message: "User not found!" });
    return res.status(200).send(user);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in xxx controller ${error}` });
  }
};

export const getUserListByShopId = async (req, res) => {
  try {
    const shopId = req.query.shopId;
    const staffs = await User.find({ shopId: shopId });
    console.log("All user : ", staffs.length);
    return res.status(200).send(staffs);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in getUserListByShopId controller ${error}` });
  }
};

export const changePassword = async (req, res) => {
  try {
    const userId = req.user.userId;
    const { oldPassword, newPassword } = req.body;
    const user = await User.findById(userId);
    if (bcrypt.compareSync(oldPassword, user.password)) {
      user.password = bcryptPassword(newPassword);
      const updatedUser = await user.save();
      console.log("password change with ", oldPassword, newPassword);
      return res.status(201).send("Password Change");
    } else {
      return res.status(404).send({ message: "Old Password incorrect!" });
    }
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in changePassword controller ${error}` });
  }
};

export async function sendEmailToSetPassword(req, res) {
  try {
    console.log("request came to send email to user ", req.body.email);
    const requestCameFromURL = `${req.header("Origin")}/`;
    const email = req.body.email;

    const user = await User.findOne({ email });

    sendConfirmationEmail(
      req.body.email,
      user.fullName,
      requestCameFromURL,
      req,
      res
    );
  } catch (error) {
    return res.status(500).send({
      message: `sendEmailToSetPassword controller error ${error.message}`,
    });
  }
}

export const forgetPassword = async (req, res) => {
  try {
    const { email, newPassword } = req.body;
    const user = await User.findOne({ email });
    user.password = bcryptPassword(newPassword);
    const updatedUser = await user.save();
    console.log("password change with ", newPassword);
    return res.status(201).send("Password Change");
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in changePassword controller ${error}` });
  }
};

export const xxx = async (req, res) => {
  try {
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in xxx controller ${error}` });
  }
};
