import mongoose from "mongoose";
import BankAccount from "../models/bankAccountModel.js";

export const addNewBankAccount = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const id = new mongoose.Types.ObjectId();
    const newBankAccount = new BankAccount({
      bankAccountId: id,
      _id: id,
      bankName: req.body.bankName,
      accountNumber: req.body.accountNumber,
      ifscCode: req.body.ifscCode,
      accountHolderName: req.body.accountHolderName,
      currentBalance: req.body.currentBalance || 0,
      shopId: shopId,
      accountType: req.body.accountType || "Saving",
    });

    const savedBankAccount = await newBankAccount.save();
    console.log("created new bank account : ", savedBankAccount);
    return res.status(201).send(savedBankAccount);
  } catch (error) {
    console.log("Error in AddNewBankAccount: ", error);
    return res.status(500).send({ message: error });
  }
};

export const updateBankAccountDetails = async (req, res) => {
  try {
    const bankAccountId = req.query.bankAccountId;
    console.log("bankAccountId", bankAccountId);
    const bankAccount = await BankAccount.findById(bankAccountId);
    if (!bankAccount) {
      return res.status(404).send({ message: "Bankacoount not found!" });
    }
    bankAccount.bankName = req.body.bankName || backAccount.bankName;
    bankAccount.accountHolderName =
      req.body.accountHolderName || bankAccount.accountHolderName;
    bankAccount.ifscCode = req.body.ifscCode || bankAccount.ifscCode;
    bankAccount.accountNumber =
      req.body.accountNumber || bankAccount.accountNumber;
    bankAccount.currentBalance =
      req.body.currentBalance || bankAccount.currentBalance;
    bankAccount.accountType = req.body.accountType || bankAccount.accountType;

    const updatedBankAccount = await bankAccount.save();
    console.log("updatedbankaccount : ", updatedBankAccount);

    return res.status(201).send(updatedBankAccount);
  } catch (error) {
    console.log("Error in updateBankAccountDetails: ", error);
    return res
      .status(500)
      .send({ message: `Error in updateBankAccountDetails ${error}` });
  }
};

export const deleteBankAccount = async (req, res) => {
  try {
    const bankAccountId = req.query.bankAccountId;
    console.log("deleteBankAccount", bankAccountId);
    const deletedAccount = await BankAccount.deleteOne({
      bankAccountId: bankAccountId,
    });
    console.log("deleted bankAccount : ", deletedAccount);

    return res.status(200).send({ message: "Deleted Bank Account" });
  } catch (error) {
    console.log("Error in deleteBankAccount: ", error);
    return res
      .status(500)
      .send({ message: `Error in deleteBankAccount ${error}` });
  }
};

export const getBankAccountDetails = async (req, res) => {
  try {
    const bankAccountId = req.query.bankAccountId;
    const bankAccount = await BankAccount.findById(bankAccountId);
    if (!bankAccount) {
      return res.status(404).send({ message: "Bank account not found!" });
    }
    return res.status(200).send(bankAccount);
  } catch (error) {
    console.log("Error in getBankAccountDetails: ", error);
    return res
      .status(500)
      .send({ message: `Error in getBankAccountDetails ${error}` });
  }
};

export const getBankAccountListByShopId = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const bankAccounts = await BankAccount.find({ shopId });
    return res.status(200).send(bankAccounts);
  } catch (error) {
    console.log("Error in getBankAccountListByShopId: ", error);
    return res
      .status(500)
      .send({ message: `Error in getBankAccountListByShopId ${error}` });
  }
};

export const xxx = async (req, res) => {
  try {
  } catch (error) {
    console.log("Error in xxx: ", error);
    return res.status(500).send({ message: `Error in xxx ${error}` });
  }
};
