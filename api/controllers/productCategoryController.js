import ProductCategory from "../models/productCategoryModel.js";

export const addNewProductCategory = async (req, res) => {
  try {
    const shopId = req.user.shopId;

    const myProductCategory = await ProductCategory.findOne({ shopId });
    if (!myProductCategory) {
      // add new
      const myProductCategory = new ProductCategory({
        shopId,
        productCategory: req.body.productCategory || [],
        priceType: req.body.priceType || [],
      });

      const savedProductCategory = await myProductCategory.save();
      console.log("addNewProductCategory : ", savedProductCategory);
      return res.status(201).send(savedProductCategory);
    }
    // else update that one
    myProductCategory.productCategory =
      req.body.productCategory || myProductCategory.productCategory;
    myProductCategory.priceType =
      req.body.priceType || myProductCategory.priceType;

    const updatedCategory = await myProductCategory.save();
    console.log("addNewProductCategory : ", updatedCategory);

    return res.status(202).send(updatedCategory);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

export const updateProductCategory = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const myProductCategory = await ProductCategory.findOne({ shopId });
    if (!myProductCategory) {
      return await addNewProductCategory(req, res);
    }
    myProductCategory.productCategory =
      req.body.productCategory || myProductCategory.productCategory;
    myProductCategory.priceType =
      req.body.priceType || myProductCategory.priceType;

    const updatedCategory = await myProductCategory.save();
    return res.status(202).send(updatedCategory);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

export const getProductCategory = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const myProductCategory = await ProductCategory.findOne({ shopId });
    if (!myProductCategory)
      return res.status(200).send({ productCategory: [], priceType: [] });

    return res.status(200).send(myProductCategory);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};
