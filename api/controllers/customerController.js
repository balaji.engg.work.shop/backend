import mongoose from "mongoose";
import Customer from "../models/customerModel.js";

export const addNewCustomer = async (req, res) => {
  try {
    console.log("addNewCustomer called! ", req.body);
    const shopId = req.user.shopId;
    const mobileNo = req.body.mobileNo;

    const customer = await Customer.findOne({ shopId, mobileNo });
    if (customer) {
      console.log("Customer all ready present in this shop! ", customer);
      return res.status(400).send({
        message: `Customer already present with this mobileNo ${mobileNo}`,
      });
    }
    console.log("creating new customer......");
    // if not present customer then create its account
    const id = new mongoose.Types.ObjectId();
    const newCustomer = new Customer({
      _id: id,
      customerId: id,
      customerName: req.body.customerName?.trim().toUpperCase(),
      mobileNo: req.body.mobileNo?.trim(),
      email: req.body.email ? req.body.email.trim().toLowerCase() : "",
      gst: req.body.gst,
      address: req.body.address,
      city: req.body.city,
      state: req.body.state,
      country: req.body.country,
      pinCode: req.body.pinCode,
      shopId,
    });

    const savedCustomer = await newCustomer.save();
    console.log("created new customer successfully ", savedCustomer);
    return res.status(201).send(savedCustomer);
  } catch (error) {
    console.log(`Error in addNewCustomer controller ${error}`);
    return res
      .status(500)
      .send({ message: `Error in addNewCustomer controller ${error}` });
  }
};

export const updateCustomerProfile = async (req, res) => {
  try {
    console.log("updateCustomerProfile called! ");
    const customerId = req.query.customerId;
    const customer = await Customer.findById(customerId);
    if (!customer)
      return res.status(404).send({ message: "Customer not found" });

    customer.customerName = req.body.customerName || customer.customerName;
    customer.email = req.body.email
      ? req.body.email.trim().toLowerCase()
      : customer.email;
    customer.mobileNo = req.body.mobileNo || customer.mobileNo;
    customer.gst = req.body.gst || customer.gst;
    customer.address = req.body.address || customer.address;
    customer.city = req.body.city || customer.city;
    customer.state = req.body.state || customer.state;
    customer.country = req.body.country || customer.country;
    customer.pinCode = req.body.pinCode || customer.pinCode;
    const updatedCustomer = await customer.save();
    console.log("Customer updated : ", updatedCustomer);
    return res.status(200).send(updatedCustomer);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in updateCustomerProfile controller ${error}` });
  }
};

export const deleteCustomer = async (req, res) => {
  try {
    console.log("deleteCustomer called!");
    const shopId = req.user.shopId;
    const customerId = req.query.customerId;
    const customers = await Customer.deleteOne({ customerId });
    console.log("customers ; ", customers);
    return res.status(200).send(customers);
  } catch (error) {
    return res.status(500).send({
      message: `Error in getCustomerListByShopId controller ${error}`,
    });
  }
};

export const getCustomerListByShopId = async (req, res) => {
  try {
    console.log("getCustomerListByShopId called!");
    const shopId = req.user.shopId;
    const customers = await Customer.find({ shopId });
    console.log("customers ; ", customers);
    return res.status(200).send(customers);
  } catch (error) {
    return res.status(500).send({
      message: `Error in getCustomerListByShopId controller ${error}`,
    });
  }
};

export const getCustomerDetailsByMobileNo = async (req, res) => {
  try {
    console.log("getCustomerDetailsByMobileNo called!");
    const mobileNo = req.query.mobileNo;
    if (!mobileNo)
      return res.status(400).send({ message: "Please send mobile no" });
    const shopId = req.user.shopId;
    const customer = await Customer.findOne({ mobileNo, shopId });
    if (!customer)
      return res.status(404).send({ message: "Customer not found!" });
    return res.status(200).send(customer);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: `Error in getCustomerDetailsByMobileNo controller ${error}`,
    });
  }
};

export const xxx = async (req, res) => {
  try {
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in xxx controller ${error}` });
  }
};
