import {
  BOTH,
  CASH,
  CREDIT,
  ONLINE,
  SELL_PRODUCT,
} from "../Constants/genericConstant.js";
import {
  decrementProductQuantity,
  incrementProductQuantity,
} from "../helpers/productHelpers.js";
import Bill from "../models/BillModel.js";
import Shop from "../models/shopModel.js";
import mongoose from "mongoose";
import { getBillNumber } from "../helpers/billHelper.js";
import moment from "moment";
import Product from "../models/productModel.js";
import Transaction from "../models/transactionModel.js";
import Customer from "../models/customerModel.js";
import {
  decrementCustomerPurchaseAmount,
  incrementCustomerPurchaseAmount,
} from "../helpers/customerHelpers.js";
import CashAccount from "../models/cashAccountModel.js";
import {
  createCashAccountForShop,
  incrementBalanceInCaseAccount,
} from "../helpers/cashAccountHelper.js";
import {
  decrementBalanceInAccount,
  incrementBalanceInAccount,
} from "../helpers/bankAccountHelpers.js";
import { getNextInvoiceNumber } from "../helpers/invoiceNumberGeneratorHelper.js";

export const addNewBill = async (req, res) => {
  try {
    const invoiceNumber = await getNextInvoiceNumber(req.user.shopId);
    const billNumber = getBillNumber(invoiceNumber);

    const shop = await Shop.findById(req.user.shopId);
    const id = new mongoose.Types.ObjectId();
    const transactionId = new mongoose.Types.ObjectId();
    let newBill = new Bill({
      _id: id,
      billNo: billNumber,
      billDate: req.body.billDate || new Date(),
      shopId: req.user.shopId,
      userId: req.user.userId,
      customerId: req.body.customerId,
      modeOfTransaction: req.body.modeOfTransaction, // Case/ Online , both
      isGST: req.body.isGST,
      isSameState: req.body.isSameState,
      total: req.body.total, // without gst
      CGSTPercent: req.body.CGSTPercent,
      SGSTPercent: req.body.SGSTPercent,
      IGSTPercent: req.body.IGSTPercent,
      cashAmount: req.body.cashAmount,
      onlineAmount: req.body.onlineAmount,
      gstAmount: req.body.gstAmount,
      discountAmount: req.body.discountAmount,
      netTotal: req.body.netTotal,
      paidAmount: req.body.paidAmount,
      products: req.body.products,
      transactionId: transactionId,
    });

    const newTransaction = new Transaction({
      _id: transactionId,
      transactionId: transactionId,
      bankTransactionId: req.body.bankTransactionId,
      billNo: newBill.billNo,
      billAmount: req.body.netTotal,
      paidAmount: req.body.paidAmount,
      mot: req.body.modeOfTransaction,
      accountNumber: req.body.accountNumber,
      customerId: req.body.customerId,
      customerName: req.body.customerName,
      transactionType: req.body.transactionType || CREDIT, //  CREDIT, DEBIT
      shopId: req.user.shopId,
      userId: req.user.userId,
      userName: req.user.fullName,
      transactionPurpose: SELL_PRODUCT,
    });
    const transaction = await newTransaction.save();
    newBill = await newBill.save();

    if (
      req.body.modeOfTransaction === ONLINE ||
      req.body.modeOfTransaction === BOTH
    ) {
      await incrementBalanceInAccount(
        req.body.accountNumber,
        req.body.onlineAmount
      );
    }

    if (
      req.body.modeOfTransaction === CASH ||
      req.body.modeOfTransaction === BOTH
    ) {
      const caseAccount = await CashAccount.findOne({
        shopId: req.user.shopId,
      });
      console.log("caseAccount ", caseAccount);
      if (!caseAccount) {
        // create cash account for this shop
        const shopCashAccount = await createCashAccountForShop(req.user.shopId);
        console.log("new caseAccount ", shopCashAccount);

        await incrementBalanceInCaseAccount(
          shopCashAccount.cashAccountNo,
          req.body.cashAmount
        );
      } else {
        await incrementBalanceInCaseAccount(
          caseAccount.cashAccountNo,
          req.body.cashAmount
        );
      }
    }

    const customer = await Customer.findById(req.body.customerId);
    if (customer)
      await incrementCustomerPurchaseAmount(
        customer.customerId,
        req.body.paidAmount
      );

    // now update product quantity
    for (let product of req.body.products) {
      if (product.productId) {
        const response = await decrementProductQuantity(
          product.productId,
          product.quantity
        );
      }
    }

    console.log("result:", {
      bill: newBill,
      shop: shop,
      transaction,
    });

    // now send the response
    return res.status(201).send({
      bill: newBill,
      shop: shop,
      customer: customer,
    });
  } catch (error) {
    console.log("Error in addNewBill", error);
    return res.status(500).send({ message: error });
  }
};

export const getBillDetails = async (req, res) => {
  try {
    const bill = await Bill.findOne({
      shopId: req.user.shopId,
      billNo: req.query.billNo,
    });
    if (!bill) return res.status(404).send({ message: "Bill Not Found" });
    const shop = await Shop.findById(req.user.shopId);
    const customer = await Customer.findById(bill.customerId);

    if (!bill) {
      return res.status(404).send({ message: "No bill detail found!" });
    }
    return res.status(200).send({ bill, shop, customer });
  } catch (error) {
    console.log("Error in addNewBill", error);
    return res.status(500).send({ message: error });
  }
};

export const getMonthlyAndTotalSell = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const startOfMonth = moment().startOf("month").toDate();
    const endOfMonth = moment().endOf("month").toDate();
    const thisMonthBillCount = await Bill.countDocuments({
      billDate: {
        $gte: startOfMonth,
        $lt: endOfMonth,
      },
      shopId,
    });
    const totalBillCount = await Bill.countDocuments({ shopId });
    const totalProductCount = await Product.countDocuments({ shopId });
    const totalCustomerCount = await Customer.countDocuments({ shopId });
    const yearSellData = await getMonthlySaleOfAYear(req, res);
    const monthSellData = await getDailySalesDataForCurrentMonth(req, res);

    return res.status(200).send({
      thisMonthBillCount,
      totalBillCount,
      totalProductCount,
      totalCustomerCount,
      yearSellData,
      monthSellData,
    });
  } catch (error) {
    console.log("Error in addNewBill", error);
    return res.status(500).send({ message: error });
  }
};

export const getMonthlySaleOfAYear = async (req, res) => {
  try {
    const shopId = new mongoose.Types.ObjectId(req.user.shopId);
    const startOfYear = moment().startOf("year").toDate();
    const endOfYear = moment().endOf("year").toDate();
    const results = await Bill.aggregate([
      {
        $match: {
          $and: [
            {
              billDate: {
                $gte: startOfYear,
                $lt: endOfYear,
              },
            },
            { shopId: shopId },
          ],
        },
      },
      {
        $group: {
          _id: {
            year: { $year: "$billDate" },
            month: { $month: "$billDate" },
          },
          totalAmount: { $sum: "$paidAmount" }, // Sum of sales amount
        },
      },

      {
        $sort: { "_id.month": 1 }, // Sort by month
      },
    ]);

    console.log("result :", results);

    // Transform results into desired format
    const formattedResults = results.map((result) => {
      const monthName = moment()
        .month(result._id.month - 1)
        .format("MMMM"); // Convert month number to month name
      return {
        month: monthName,
        totalSell: result.totalAmount,
      };
    });
    return formattedResults;
  } catch (error) {
    console.error("Error fetching monthly sales data:", error);
  }
};

export const getDailySalesDataForCurrentMonth = async (req, res) => {
  try {
    const shopId = new mongoose.Types.ObjectId(req.user.shopId);
    const startOfMonth = moment().startOf("month").toDate();
    const endOfMonth = moment().endOf("month").toDate();

    const results = await Bill.aggregate([
      {
        $match: {
          $and: [
            {
              billDate: {
                $gte: startOfMonth,
                $lt: endOfMonth,
              },
            },
            { shopId: shopId },
          ],
        },
      },
      {
        $group: {
          _id: {
            year: { $year: "$billDate" },
            month: { $month: "$billDate" },
            day: { $dayOfMonth: "$billDate" },
          },
          totalAmount: { $sum: "$paidAmount" }, // Sum of sales amount
        },
      },
      {
        $sort: { "_id.day": 1 }, // Sort by day
      },
    ]);

    // Transform results into desired format
    const formattedResults = results.map((result) => {
      const date = moment({
        year: result._id.year,
        month: result._id.month - 1, // moment.js months are 0-indexed
        day: result._id.day,
      }).format("M/D/YYYY"); // Format as M/D/YYYY

      return {
        date: date,
        totalSell: result.totalAmount,
      };
    });

    return formattedResults;
  } catch (error) {
    console.error("Error fetching daily sales data:", error);
  }
};

export const deleteSellTransaction = async (req, res) => {
  try {
    console.log("deleteSellTransaction called!");
    const transaction = await Transaction.findById(req.query.transactionId);
    const oldBill = await Bill.findOne({ billNo: transaction.billNo });

    if (
      transaction.modeOfTransaction === ONLINE ||
      transaction.modeOfTransaction === BOTH
    ) {
      await decrementBalanceInAccount(
        transaction.accountNumber,
        oldBill.onlineAmount
      );
    }

    if (
      transaction.modeOfTransaction === ONLINE ||
      transaction.modeOfTransaction === BOTH
    ) {
      const caseAccount = await CashAccount.findOne({
        shopId: req.user.shopId,
      });
      console.log("caseAccount ", caseAccount);

      await decrementBalanceInAccount(
        caseAccount.cashAccountNo,
        oldBill.cashAmount
      );
    }

    const customer = await Customer.findById(req.body.customerId);
    if (customer)
      await decrementCustomerPurchaseAmount(
        customer.customerId,
        req.body.paidAmount
      );

    // now update product quantity
    for (let product of oldBill.products) {
      if (product.productId) {
        const response = await incrementProductQuantity(
          product.productId,
          product.quantity
        );
      }
    }

    const deletedBill = await Bill.findByIdAndDelete(oldBill._id);
    const deletedTransaction = await Transaction.findByIdAndDelete(
      transaction._id
    );

    console.log("dddddd : ", deletedBill, deletedTransaction);

    // now send the response
    return res.status(200).send({
      message: "Transaction deleted!",
    });
  } catch (error) {
    console.log("Error in addNewBill", error);
    return res.status(500).send({ message: error });
  }
};
