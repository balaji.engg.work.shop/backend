import {
  PURCHASE_PRODUCT,
  SELL_PRODUCT,
  SELL_RETURN,
} from "../Constants/genericConstant.js";
import { transaction } from "../Data/transaction.js";
import Transaction from "../models/transactionModel.js";

export const getSellTransactionListByShopId = async (req, res) => {
  try {
    console.log("getSellTransactionListByShopId called", req.query);
    const shopId = req.user.shopId;
    const transactionList = (
      await Transaction.find(
        { shopId, transactionPurpose: SELL_PRODUCT },
        transaction
      )
    ).reverse();
    return res.status(200).send(transactionList);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

export const getPurchaseTransactionListByShopId = async (req, res) => {
  try {
    console.log("getSellTransactionListByShopId called", req.query);
    const shopId = req.user.shopId;
    const transactionList = (
      await Transaction.find(
        { shopId, transactionPurpose: PURCHASE_PRODUCT },
        transaction
      )
    ).reverse();
    return res.status(200).send(transactionList);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

export const getSellReturnTransactionListByShopId = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const transactionList = (
      await Transaction.find(
        { shopId, transactionPurpose: SELL_RETURN },
        transaction
      )
    ).reverse();
    return res.status(200).send(transactionList);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

export const getTransactionListByCustomerId = async (req, res) => {
  try {
    console.log("getTransactionListByCustomerId called", req.query);
    const { customerId } = req.query;
    const transactionList = (
      await Transaction.find(
        { customerId, transactionPurpose: SELL_PRODUCT },
        transaction
      )
    ).reverse();
    return res.status(200).send(transactionList);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

export const getSellReturnTransactionListByCustomerId = async (req, res) => {
  try {
    const { customerId } = req.query;
    const transactionList = (
      await Transaction.find(
        { customerId, transactionPurpose: SELL_RETURN },
        transaction
      )
    ).reverse();
    console.log("transactionList", transactionList);
    return res.status(200).send(transactionList);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};

export const getTransactionListBySupplierId = async (req, res) => {
  try {
    console.log("getTransactionListBySupplierId called", req.query);
    const { supplierId } = req.query;
    console.log("supplierId : ", supplierId);
    const transactionList = (
      await Transaction.find(
        { supplierId, transactionPurpose: PURCHASE_PRODUCT },
        transaction
      )
    ).reverse();
    console.log("transactionList", transactionList);
    return res.status(200).send(transactionList);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: error });
  }
};
