import mongoose from "mongoose";
import Supplier from "../models/supplierModel.js";

export const addNewSupplier = async (req, res) => {
  try {
    console.log("addNewSupplier called! ", req.body);
    const shopId = req.user.shopId;
    const mobileNo = req.body.mobileNo;

    const supplier = await Supplier.findOne({ shopId, mobileNo });
    if (supplier) {
      console.log("Supplier all ready present in this shop! ", supplier);
      return res.status(400).send({
        message: `Supplier already present with this mobileNo ${mobileNo}`,
      });
    }
    console.log("creating new supplier......");
    // if not present suppler then create its account
    const id = new mongoose.Types.ObjectId();
    const newSupplier = new Supplier({
      _id: id,
      supplierId: id,
      supplierName: req.body.supplierName?.trim().toUpperCase(),
      mobileNo: req.body.mobileNo?.trim(),
      email: req.body.email
        ? req.body.email.trim().toLowerCase()
        : req.body.email,
      gst: req.body.gst,
      address: req.body.address,
      city: req.body.city,
      state: req.body.state,
      country: req.body.country,
      pinCode: req.body.pinCode,
      shopId,
    });

    const saveSupplier = await newSupplier.save();
    console.log("created new suppler successfully ", saveSupplier);
    return res.status(201).send(saveSupplier);
  } catch (error) {
    console.log(`Error in addNewSuppler controller ${error}`);
    return res
      .status(500)
      .send({ message: `Error in addNewSuppler controller ${error}` });
  }
};

export const updateSupplierProfile = async (req, res) => {
  try {
    console.log("updateSupplierProfile called! ");
    const supplierId = req.query.supplierId;
    const supplier = await Supplier.findById(supplierId);
    if (!supplier)
      return res.status(404).send({ message: "Supplier not found" });

    supplier.supplierName = req.body.supplierName || supplier.supplierName;
    supplier.email = req.body.email
      ? req.body.email.trim().toLowerCase()
      : supplier.email;
    supplier.mobileNo = req.body.mobileNo || supplier.mobileNo;
    supplier.gst = req.body.gst || supplier.gst;
    supplier.address = req.body.address || supplier.address;
    supplier.city = req.body.city || supplier.city;
    supplier.state = req.body.state || supplier.state;
    supplier.country = req.body.country || supplier.country;
    supplier.pinCode = req.body.pinCode || supplier.pinCode;
    const updatedSupplier = await supplier.save();
    console.log("Supplier updated : ", updatedSupplier);
    return res.status(200).send(updatedSupplier);
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send({ message: `Error in updateSupplierProfile controller ${error}` });
  }
};

export const getSupplierListByShopId = async (req, res) => {
  try {
    console.log("getSupplierListByShopId called!");
    const shopId = req.user.shopId;
    const suppliers = await Supplier.find({ shopId });
    console.log("suppliers ; ", suppliers);
    return res.status(200).send(suppliers);
  } catch (error) {
    return res.status(500).send({
      message: `Error in getSupplierListByShopId controller ${error}`,
    });
  }
};

export const getSupplierBySupplierId = async (req, res) => {
  try {
    console.log("getSupplerDetailsByMobileNo called!");
    const supplierId = req.query.supplierId;
    if (!supplierId)
      return res.status(400).send({ message: "Please send mobile no" });
    const shopId = req.user.shopId;
    const supplier = await Supplier.findOne({ supplierId, shopId });
    if (!supplier)
      return res.status(404).send({ message: "Supplier not found!" });
    return res.status(200).send(supplier);
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: `Error in getSupplerDetailsByMobileNo controller ${error}`,
    });
  }
};

export const xxx = async (req, res) => {
  try {
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in xxx controller ${error}` });
  }
};
