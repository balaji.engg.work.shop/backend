import mongoose, { mongo } from "mongoose";
import Product from "../models/productModel.js";
import { incrementProductQuantity } from "../helpers/productHelpers.js";
import DailyManufacture from "../models/dailyManufactureModel.js";

export const addNewProduct = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    // const result = await Product.updateMany(
    //   {}, // Empty filter to match all products
    //   [
    //     {
    //       $set: {
    //         description: {
    //           $toUpper: { $trim: { input: "$description" } }, // Trim and convert to uppercase
    //         },
    //       },
    //     }, // MongoDB's aggregation pipeline operator $toUpper
    //   ]
    // );
    // console.log("result : ", result);
    const oldProduct = await Product.findOne({
      shopId: shopId,
      description: req.body.description?.trim().toUpperCase(),
    });

    if (oldProduct)
      return res.status(409).send({ message: "Product all ready present" });
    let productCode = 1000;
    const product = await Product.countDocuments({ shopId: shopId });
    productCode = productCode + product + 1;
    const id = new mongoose.Types.ObjectId();
    const newProduct = new Product({
      productId: id,
      _id: id,
      productCode: productCode,
      HSN_SAC_Code: req.body.HSN_SAC_Code,
      category: req.body.category,
      description: req.body.description?.trim().toUpperCase(),
      weightPerPiece: req.body.weightPerPiece || 0,
      weightUnit: req.body.weightUnit || "Gram",
      quantity: req.body.quantity,
      priceType: req.body.priceType,
      sellingPrice: req.body.sellingPrice,
      MRP: req.body.MRP,
      purchasingPrice: req.body.purchasingPrice,
      images: req.body.images, // store Multiple images
      shopId: shopId,
      brand: req.body.brand,
    });

    const savedProduct = await newProduct.save();
    console.log("saved product : ", savedProduct);
    return res.status(201).send(savedProduct);
  } catch (error) {
    console.log("errrr", error);
    return res
      .status(500)
      .send({ message: `Error in addNewProduct controller ${error}` });
  }
};

export const updatedProduct = async (req, res) => {
  try {
    const productId = req.query.productId;

    const product = await Product.findById(productId);

    if (!product) return res.status(404).send("Product not found");

    product.HSN_SAC_Code = req.body.HSN_SAC_Code || product.HSN_SAC_Code;
    product.category = req.body.category || product.category;
    product.description =
      req.body.description?.trim().toUpperCase() || product.description;
    product.weightPerPiece = req.body.weightPerPiece || product.weightPerPiece;
    product.weightUnit = req.body.weightUnit || product.weightUnit;
    product.quantity = req.body.quantity || product.quantity;
    product.priceType = req.body.priceType || product.priceType;
    product.sellingPrice = req.body.sellingPrice || product.sellingPrice;
    product.MRP = req.body.MRP || product.MRP;
    product.purchasingPrice =
      req.body.purchasingPrice || product.purchasingPrice;
    product.images = req.body.images || product.images; // store Multiple images
    product.brand = req.body.brand || product.brand;

    const updatedProduct = await product.save();
    console.log("updated product : ", updatedProduct);
    return res.status(200).send(updatedProduct);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in updatedProduct controller ${error}` });
  }
};

export const deleteProduct = async (req, res) => {
  try {
    const productId = req.query.productId;
    console.log("productId", productId);
    const product = await Product.deleteOne({ productId: productId });
    console.log("product : ", product);
    return res.status(200).send({ message: "product deleted!" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in deleteProduct controller ${error}` });
  }
};

export const deleteAllProducts = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const product = await Product.deleteMany({ shopId: shopId });
    console.log("product : ", product);
    return res.status(200).send({ message: "products deleted!" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in deleteProduct controller ${error}` });
  }
};

export const getAllProductList = async (req, res) => {
  try {
    const products = await Product.find();
    return res.status(200).send(products);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in getAllProductByShopId controller ${error}` });
  }
};

export const getAllProductByShopId = async (req, res) => {
  try {
    console.log("shopId  getAllProductByShopId called ");

    const shopId = req.query.shopId;
    console.log("shopId  getAllProductByShopId: ", shopId);
    const products = await Product.find({ shopId: shopId });
    return res.status(200).send(products);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in getAllProductByShopId controller ${error}` });
  }
};

export const getProductDetails = async (req, res) => {
  try {
    const productId = req.params.id;
    const product = await Product.findById(productId);
    if (!product) return res.status(404).send({ message: "Product not found" });
    return res.status(200).send(product);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in getProductDetails controller ${error}` });
  }
};

export const getProductListByCategory = async (req, res) => {
  try {
    console.log("category to search :  ", req.params.category);
    const productList = await Product.find({
      category: req.params.category,
      shopId: req.params.shopId,
    });
    console.log("getProductListByCategory : ", productList);
    if (productList.length > 0) {
      res.status(200).send(productList);
    } else {
      res.status(404).send({
        message:
          "No product found with this category, try with different category!",
      });
    }
  } catch (error) {
    res.status(501).send({
      message: `Error in getProductListByCategory controller ${error}`,
    });
  }
};

export const productListByProductName = async (req, res) => {
  try {
    console.log("productListByProductName to search :  ");
    const productName = req.query.productName;

    const productNameFilter = {
      description: { $not: { $regex: productName, $options: "i" } },
    }; // remove screens filter which name have sample

    const productList = await Product.find({
      ...productNameFilter,
    });

    res.status(200).send(productList);
  } catch (error) {
    res.status(501).send({
      message: `Error in productListByProductName controller ${error}`,
    });
  }
};

export const addProductInBulk = async (req, res) => {
  try {
    console.log("addProductInBulk ", req.body);
    let allProduct = req.body;
    if (allProduct?.length === 0) {
      return res.status(400).send({ message: "product not present" });
    }
    let productCode = 1000;
    const productCount = await Product.countDocuments({
      shopId: req.user.shopId,
    });

    productCode = productCode + productCount + 1;

    allProduct = allProduct.map((product, index) => {
      const id = new mongoose.Types.ObjectId();
      return {
        ...product,
        description: product.description?.trim().toUpperCase(),
        shopId: req.user.shopId,
        productCode: productCode + index,
        productId: id,
        _id: id,
      };
    });

    const products = await Product.insertMany(allProduct);
    return res.status(201).send(products);
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send({ message: `Error in addProductInBulk controller ${error}` });
  }
};

export const dailyManufactureProducts = async (req, res) => {
  try {
    const list = req.body.products;
    const shopId = req.user.shopId;
    for (let data of list) {
      const saveDailyManufacture = new DailyManufacture({
        shopId,
        productId: data.productId,
        quantity: data.quantity,
        date: req.body.date,
        name: req.user.fullName,
      });
      await saveDailyManufacture.save();
      const response = await incrementProductQuantity(
        data.productId,
        data.quantity
      );
    }
    console.log("dailyManufactureProducts success!");
    return res.status(202).send("Successfully added");
  } catch (error) {
    console.log("error : ", error);
    return res
      .status(500)
      .send({ message: `Error in xxx controller ${error}` });
  }
};

export const getDailyManufactureProductList = async (req, res) => {
  try {
    const shopId = req.user.shopId;
    const products = await Product.find(
      { shopId },
      { description: 1, productId: 1 }
    );
    const result = await DailyManufacture.find(
      { shopId },
      { date: 1, name: 1, productId: 1, date: 1, quantity: 1 }
    );
    let finalResult = [];
    for (let data of result) {
      let product = products.find((data) => data.productId == data.productId);
      finalResult.push({
        ...data._doc,
        description: product.description,
      });
    }
    return res.status(200).send(finalResult);
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send({ message: `Error in xxx controller ${error}` });
  }
};

export const xxx = async (req, res) => {
  try {
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Error in xxx controller ${error}` });
  }
};
