import mongoose from "mongoose";
import {
  BOTH,
  CASH,
  DEBIT,
  ONLINE,
  SELL_RETURN,
} from "../Constants/genericConstant.js";
import { decrementBalanceInAccount } from "../helpers/bankAccountHelpers.js";
import { getSellReturnBillNumber } from "../helpers/billHelper.js";
import { decrementBalanceInCaseAccount } from "../helpers/cashAccountHelper.js";
import { decrementCustomerPurchaseAmount } from "../helpers/customerHelpers.js";
import { getNextInvoiceNumberForSellReturn } from "../helpers/invoiceNumberGeneratorHelper.js";
import { incrementProductQuantity } from "../helpers/productHelpers.js";
import CashAccount from "../models/cashAccountModel.js";
import Customer from "../models/customerModel.js";
import SellReturn from "../models/sellReturnModel.js";
import Shop from "../models/shopModel.js";
import Transaction from "../models/transactionModel.js";

export const addSellReturn = async (req, res) => {
  try {
    const shop = await Shop.findById(req.user.shopId);
    const id = new mongoose.Types.ObjectId();
    const transactionId = new mongoose.Types.ObjectId();
    const invoiceNumber = await getNextInvoiceNumberForSellReturn(
      req.user.shopId
    );
    const billNumber = getSellReturnBillNumber(invoiceNumber);
    let sellReturn = new SellReturn({
      _id: id,
      oldBillNo: req.body.oldBillNo,
      billNo: billNumber,
      billDate: req.body.billDate || new Date(),
      shopId: req.user.shopId,
      userId: req.user.userId,
      customerId: req.body.customerId,
      modeOfTransaction: req.body.modeOfTransaction, // Case/ Online , both
      isGST: req.body.isGST,
      isSameState: req.body.isSameState,
      total: req.body.total, // without gst
      CGSTPercent: req.body.CGSTPercent,
      SGSTPercent: req.body.SGSTPercent,
      IGSTPercent: req.body.IGSTPercent,
      cashAmount: req.body.cashAmount,
      onlineAmount: req.body.onlineAmount,
      gstAmount: req.body.gstAmount,
      discountAmount: req.body.discountAmount,
      netTotal: req.body.netTotal,
      paidAmount: req.body.paidAmount,
      products: req.body.products,
      transactionId: transactionId,
    });

    const newTransaction = new Transaction({
      _id: transactionId,
      transactionId: transactionId,
      bankTransactionId: req.body.bankTransactionId,
      billNo: sellReturn.billNo,
      billAmount: req.body.netTotal,
      paidAmount: req.body.paidAmount,
      mot: req.body.modeOfTransaction,
      accountNumber: req.body.accountNumber,
      customerId: req.body.customerId,
      customerName: req.body.customerName,
      transactionType: req.body.transactionType || DEBIT, //  CREDIT, DEBIT
      shopId: req.user.shopId,
      userId: req.user.userId,
      userName: req.user.fullName,
      transactionPurpose: SELL_RETURN,
    });
    const transaction = await newTransaction.save();
    sellReturn = await sellReturn.save();

    if (
      req.body.modeOfTransaction === ONLINE ||
      req.body.modeOfTransaction === BOTH
    ) {
      await decrementBalanceInAccount(
        req.body.accountNumber,
        req.body.onlineAmount
      );
    }

    if (
      req.body.modeOfTransaction === CASH ||
      req.body.modeOfTransaction === BOTH
    ) {
      const caseAccount = await CashAccount.findOne({
        shopId: req.user.shopId,
      });
      // console.log("caseAccount ", caseAccount);
      if (!caseAccount) {
        // create cash account for this shop
        const shopCashAccount = await createCashAccountForShop(req.user.shopId);
        // console.log("new caseAccount ", shopCashAccount);

        await decrementBalanceInCaseAccount(
          shopCashAccount.cashAccountNo,
          req.body.cashAmount
        );
      } else {
        await decrementBalanceInCaseAccount(
          caseAccount.cashAccountNo,
          req.body.cashAmount
        );
      }
    }

    const customer = await Customer.findById(req.body.customerId);
    if (customer)
      await decrementCustomerPurchaseAmount(
        customer.customerId,
        req.body.paidAmount
      );

    // console.log("req.body.products : ", req.body.products);
    // now update product quantity
    for (let product of req.body.products) {
      if (product.productId) {
        const response = await incrementProductQuantity(
          product.productId,
          product.quantity
        );
      }
    }

    console.log("result:", {
      bill: sellReturn,
      shop: shop,
      transaction,
    });

    // now send the response
    return res.status(201).send({
      bill: sellReturn,
      shop: shop,
      customer: customer,
    });
  } catch (error) {
    // console.log("Error in addsellReturn", error);
    return res.status(500).send({ message: error });
  }
};

export const getSellReturnDetails = async (req, res) => {
  try {
    const bill = await SellReturn.findOne({
      shopId: req.user.shopId,
      billNo: req.query.billNo,
    });
    if (!bill) return res.status(404).send({ message: "Bill Not Found" });
    const shop = await Shop.findById(req.user.shopId);
    const customer = await Customer.findById(bill.customerId);

    if (!bill) {
      return res.status(404).send({ message: "No bill detail found!" });
    }
    return res.status(200).send({ bill, shop, customer });
  } catch (error) {
    console.log("Error in addNewBill", error);
    return res.status(500).send({ message: error });
  }
};
