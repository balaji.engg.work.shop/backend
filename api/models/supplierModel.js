import mongoose from "mongoose";
const Schema = mongoose.Schema;

const supplierSchema = new Schema(
  {
    supplierId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    supplierName: {
      type: String,
      required: true,
    },

    mobileNo: { type: String, required: true },
    email: { type: String },
    address: { type: String },
    city: {
      type: String,
    },
    state: {
      type: String,
    },
    country: {
      type: String,
    },
    pinCode: {
      type: String,
    },
    totalPurchaseAmount: {
      type: Number,
      default: 0,
    },
    avatar: {
      type: String, // profile image url
      default:
        "https://static.vecteezy.com/system/resources/previews/000/439/863/original/vector-users-icon.jpg",
    },
    gst: { type: String },
  },
  {
    timestamps: true,
  }
);

const Supplier = mongoose.model("supplier", supplierSchema);
export default Supplier;
