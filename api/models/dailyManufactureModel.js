import mongoose from "mongoose";
const Schema = mongoose.Schema;

const dailyManufacture = new Schema(
  {
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Product",
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
    },
    date: {
      type: Date,
      default: new Date(),
    },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const DailyManufacture = mongoose.model("dailyManufacture", dailyManufacture);
export default DailyManufacture;
