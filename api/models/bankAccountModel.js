import mongoose from "mongoose";
import { SAVING } from "../Constants/genericConstant.js";
const Schema = mongoose.Schema;

const bankSchema = new Schema(
  {
    bankAccountId: { type: mongoose.Schema.Types.ObjectId },
    bankName: {
      type: String,
      required: true,
    },
    accountNumber: {
      type: String,
      required: true,
    },
    ifscCode: {
      type: String,
      required: true,
    },
    accountType: {
      type: String,
      default: SAVING, // Saving , Current
    },
    accountHolderName: {
      type: String,
      required: true,
    },
    currentBalance: { type: Number, default: 0 },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    // transactions: [transactionsSchema],
  },
  {
    timestamps: true,
  }
);

const BankAccount = mongoose.model("bankAccount", bankSchema);
export default BankAccount;
