import mongoose from "mongoose";
import { ADMIN, MALE, STAFF, SU } from "../Constants/userConstant.js";
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    // value before @ will be userName
    userName: {
      type: String,
      required: true,
      default: "test",
    },
    fullName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      default: "aaaaaaaaa@gmail.com",
    },
    mobileNo: {
      type: String,
      default: "9999999999",
    },
    avatar: {
      type: String, // profile image url
      default:
        "https://static.vecteezy.com/system/resources/previews/000/439/863/original/vector-users-icon.jpg",
    },
    password: {
      type: String, // hashpassword -> 1. first i will try with real password then hash
      required: true,
    },
    gender: { type: String, default: MALE },
    userRole: {
      type: String,
      required: true,
      enum: [ADMIN, STAFF, SU],
      default: STAFF,
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },
    isStaff: {
      type: Boolean,
      default: true,
    },
    isSU: {
      type: Boolean,
      default: false,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop", // F.K
      default: null, // for su yser
      required: true,
    },
    aadhaarNo: { type: String, default: "" },
    shopName: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("user", userSchema);
export default User;
