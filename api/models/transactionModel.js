import mongoose from "mongoose";
import { CASH, CREDIT, ONLINE } from "../Constants/genericConstant.js";
const Schema = mongoose.Schema;

const transactionSchema = new Schema(
  {
    transactionId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    transactionDate: {
      type: Date,
      required: true,
      default: new Date(),
    },
    transactionPurpose: {
      type: String,
    },
    bankTransactionId: {
      type: String,
    },
    accountNumber: { type: String },
    billNo: {
      type: String,
      default: null,
      ref: "Bill",
    },
    billAmount: {
      type: Number,
      default: 0.0,
    },
    paidAmount: {
      type: Number,
      default: 0.0,
    },
    mot: { type: String, default: CASH }, // mode of transaction

    customerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Customer",
      default: null,
    },
    customerName: {
      type: String,
    },
    supplierName: {
      type: String,
    },
    supplierId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Supplier",
      default: null,
    },
    transactionType: {
      type: String,
      default: CREDIT,
    },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    userName: {
      type: String,
    },
  },
  { timestamps: true }
);

const Transaction = mongoose.model("transactions", transactionSchema);
export default Transaction;
