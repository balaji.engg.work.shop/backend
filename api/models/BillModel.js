import mongoose from "mongoose";
const Schema = mongoose.Schema;

const sellProductSchema = new Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product",
    default: null,
  },
  HSN_SAC_Code: {
    type: Number,
  },
  description: {
    type: String,
    required: true,
  },
  priceType: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    default: 0,
  },
  price: {
    type: Number,
    default: 0.0,
  },
  total: {
    type: Number,
    default: 0.0,
  },
});

const billSchema = new Schema(
  {
    billNo: {
      type: String,
      required: true,
    },
    billDate: { type: Date, default: new Date(), required: true },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    customerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Customer",
      required: true,
    },
    modeOfTransaction: {
      type: String,
    },
    isGST: {
      type: Boolean,
      default: false,
    },
    isSameState: {
      type: Boolean,
      default: false,
    },
    customerGSTNo: {
      type: String,
    },
    transactionId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Transaction",
    },

    total: { type: Number, default: 0.0 }, // without gst
    CGSTPercent: { type: Number, default: 0.0 },
    SGSTPercent: { type: Number, default: 0.0 },
    IGSTPercent: { type: Number, default: 0.0 },

    gstAmount: { type: Number, default: 0.0 },
    netTotal: { type: Number, default: 0.0 },
    onlineAmount: {
      type: Number,
      default: 0.0,
    },
    cashAmount: {
      type: Number,
      default: 0.0,
    },
    paidAmount: { type: Number, default: 0.0 },
    discountAmount: { type: Number, default: 0.0 },
    products: [sellProductSchema],
  },
  { timestamps: true }
);

const Bill = mongoose.model("bill", billSchema);
export default Bill;
