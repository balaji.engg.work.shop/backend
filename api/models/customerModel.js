import mongoose from "mongoose";
const Schema = mongoose.Schema;

const transactionsSchemasSchema = new Schema({
  date: { type: String, default: new Date() },
  amount: { type: Number },
  transactionsType: { type: String }, // CRADIT , DABIT
  currentBalance: { type: Number },
  paymentMode: { type: String }, // CASH , BANK
});

const customerSchema = new Schema(
  {
    customerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    customerName: {
      type: String,
      required: true,
    },

    mobileNo: { type: String, required: true },
    email: { type: String },
    address: { type: String },
    city: {
      type: String,
    },
    state: {
      type: String,
    },
    country: {
      type: String,
    },
    pinCode: {
      type: String,
    },
    totalPurchaseAmount: {
      type: Number,
      default: 0,
    },
    avatar: {
      type: String, // profile image url
      default:
        "https://static.vecteezy.com/system/resources/previews/000/439/863/original/vector-users-icon.jpg",
    },
    gst: { type: String },
  },
  {
    timestamps: true,
  }
);

const Customer = mongoose.model("customer", customerSchema);
export default Customer;
