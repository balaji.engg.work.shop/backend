import mongoose, { Model, Schema } from "mongoose";

const productCategorySchema = new Schema({
  productCategory: [String],
  shopId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Shop",
    required: true,
  },
  priceType: [String],
});

const ProductCategory = mongoose.model(
  "ProductCategory",
  productCategorySchema
);
export default ProductCategory;
