import mongoose from "mongoose";
const Schema = mongoose.Schema;

const invoiceNumberSchema = new Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "Shop" },
  sequence_value: { type: Number, default: 0 },
});

const sellReturnBillNumberSchema = new Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, required: true, ref: "Shop" },
  sequence_value: { type: Number, default: 0 },
});

const InvoiceNumber = mongoose.model("Counter", invoiceNumberSchema);
export const SellReturnInvoiceNumber = mongoose.model(
  "sellReturnInvoiceNumber",
  sellReturnBillNumberSchema
);

export default InvoiceNumber;
