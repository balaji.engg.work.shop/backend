import mongoose from "mongoose";
const Schema = mongoose.Schema;

const sellProductSchema = new Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Product",
    default: null,
  },
  description: {
    type: String,
    required: true,
  },
  priceType: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    default: 0,
  },
  price: {
    type: Number,
    default: 0.0,
  },
  total: {
    type: Number,
    default: 0.0,
  },
});

const purchaseBillSchema = new Schema(
  {
    billNo: {
      type: String,
      required: true,
    },
    billDate: { type: Date, default: new Date(), required: true },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    supplierId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Supplier",
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    modeOfTransaction: {
      type: String,
    },
    transactionId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Transaction",
    },
    onlineAmount: {
      type: Number,
      default: 0.0,
    },
    cashAmount: {
      type: Number,
      default: 0.0,
    },
    total: { type: Number, default: 0.0 }, // without gst
    gstPercent: { type: Number, default: 0.0 },

    gstAmount: { type: Number, default: 0.0 },
    discountAmount: { type: Number, default: 0.0 },
    netTotal: { type: Number, default: 0.0 },
    paidAmount: { type: Number, default: 0.0 },
    products: [sellProductSchema],
  },
  { timestamps: true }
);

const PurchaseBill = mongoose.model("purchaseBill", purchaseBillSchema);
export default PurchaseBill;
