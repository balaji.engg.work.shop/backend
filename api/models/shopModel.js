import mongoose from "mongoose";
const Schema = mongoose.Schema;

const shop = new Schema(
  {
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    shopName: {
      type: String,
      required: true,
    },
    shopLogo: {
      type: String,
    },
    address: { type: String },
    city: {
      type: String,
    },
    state: {
      type: String,
    },
    country: {
      type: String,
    },
    pinCode: {
      type: String,
    },
    shopImages: [
      {
        type: String,
      },
    ],
    ownerName: { type: String, required: true },
    // ownerId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    geoLocation: {
      lat: { type: Number, default: 25.556044 },
      lng: { type: Number, default: 84.660332 }, //
    },
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
    gst: { type: String },
    shopEmail: { type: String, required: true }, // unique
    dummyPassword: { type: String, default: "" }, // it will use to send email
    shopMobileNo: { type: String, required: true }, // sending whatsapp sms to customer
    shopOpeningTime: { type: Date },
    shopClosingTime: { type: Date },
  },

  {
    timestamps: true,
  }
);

const Shop = mongoose.model("shop", shop);
export default Shop;
