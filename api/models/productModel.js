import mongoose from "mongoose";
const Schema = mongoose.Schema;

const reviewDetail = new Schema({
  customerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Customer",
    required: true,
  },
  name: { type: String },
  rating: { type: Number, default: 0 },
  comment: { type: String, required: true },
});

const offerList = new Schema({
  offerName: { type: String, required: true },
  offerDescription: { type: String, required: true },
});

const productSchema = new Schema(
  {
    productId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    productCode: {
      // unique
      type: Number,
      required: true,
    },
    HSN_SAC_Code: {
      type: Number,
    },
    category: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    weightPerPiece: {
      type: Number,
      default: 0,
    },
    weightUnit: {
      type: String,
      default: "Gram",
    },
    quantity: {
      type: Number,
      default: 0,
      required: true,
    },
    priceType: {
      type: String,
      required: true,
    },
    purchasingPrice: {
      type: Number,
      default: 0.0,
      required: true,
    },
    MRP: {
      type: Number,
      default: 0.0,
      required: true,
    },
    sellingPrice: {
      type: Number,
      default: 0.0,
      required: true,
    },
    images: [String], // store Multiple images
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    brand: { type: String, default: "BSC" },

    // future requirement
    // liked: { type: Number, default: 0 },
    // reviews: [reviewDetail],
    // ratings: { type: Number, default: 0 },
    // numberOfReviews: { type: Number, default: 0 },
    // offers: [offerList],
  },
  {
    timestamps: true,
  }
);

const Product = mongoose.model("product", productSchema);
export default Product;
