import mongoose from "mongoose";
const Schema = mongoose.Schema;

const cashAccountSchema = new Schema(
  {
    cashAccountNo: { type: mongoose.Schema.Types.ObjectId, required: true },
    currentBalance: { type: Number, default: 0 },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const CashAccount = mongoose.model("cashAccount", cashAccountSchema);
export default CashAccount;
