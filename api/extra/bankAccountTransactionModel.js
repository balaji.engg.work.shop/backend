import mongoose from "mongoose";
const Schema = mongoose.Schema;

const bankAccountTransactionsSchema = new Schema({
    
    bankAccount: { type: mongoose.Schema.Types.ObjectId, ref: "BankAccount", required: true },
    date: { type: String, default: new Date() },
    amount: { type: Number },
    transactionType: { type: String }, // CRADIT , DABIT
    currentBalance: { type: Number },
    transactionsId: { type: String },
    purpose: { type: String }, // customer, supplier , other
    description: { type: String , required  :  true},
    customer: { type: mongoose.Schema.Types.ObjectId, ref: "Customer" },
    supplier: { type: mongoose.Schema.Types.ObjectId, ref: "Customer" },
    
});

const BankAccountTransaction = mongoose.model('bankAccountTransaction', bankAccountTransactionsSchema);
export default BankAccountTransaction;