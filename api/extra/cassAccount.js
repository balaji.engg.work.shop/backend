import mongoose from "mongoose";
const Schema = mongoose.Schema;

const transactionsSchema = new Schema({
  date: { type: String, default: new Date() },
  amount: { type: Number },
  type: { type: String }, // CRADIT , DABIT
  currentBalance: { type: Number },
  transactionsId: { type: String },
  cutomer: { type: mongoose.Schema.Types.ObjectId, ref: "Customer" },
  supplier: { type: mongoose.Schema.Types.ObjectId, ref: "Customer" },
});

const caseAccountSchema = new Schema(
  {
    ShopName: { type: string, required: true },
    currentAmmount: { type: Number, default: 0 },
    shopId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Shop",
      required: true,
    },
    transactions: [transactionsSchema],
  },
  {
    timestamps: true,
  }
);

const CashAccount = mongoose.model("caseAccount", caseAccountSchema);
export default CashAccount;
